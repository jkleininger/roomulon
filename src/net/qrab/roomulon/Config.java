package net.qrab.roomulon;

import java.awt.Dimension;
import java.awt.geom.Area;
import java.awt.geom.Line2D;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public abstract class Config {

  protected static Random    random        = new Random();

  protected static Dimension maxRoom       = new Dimension(80, 60);
  protected static Dimension minRoom       = new Dimension(30, 20);
  protected static double    hallWd        = 10;
  protected static int       maxRooms      = 20;
  protected static boolean   rotate        = true;
  protected static int       sepMargin     = 5;

  // player speed
  protected static double    acceleration  = .001;
  protected static double    deceleration  = .995;
  protected static double    rAcceleration = .002;
  protected static double    rDeceleration = .500;
  protected static double    maxspeed      = .050;
  protected static double    rotation      = .035;

  protected static double randomRange(double min, double max) {
    return (random.nextDouble() * (max - min)) + min;
  }

  protected static boolean intersects(Area a, Area b) {
    if (a.getBounds2D().intersects(b.getBounds2D())) {
      Area scrapArea = new Area(a);
      scrapArea.intersect(b);
      return !scrapArea.isEmpty();
    }
    return false;
  }

  protected static boolean contains(Area container, Area containee) {
    if (container.contains(containee.getBounds2D()))
      return true;
    Area tmpContainer = new Area(container);
    tmpContainer.add(containee);
    return tmpContainer.equals(container);
  }

  protected static boolean intersects(Area a, Area b, int margin) {
    return (getBoundsMargin(a, margin).intersects(getBoundsMargin(b, margin)));
  }

  protected static Rectangle2D getBoundsMargin(Area a, int m) {
    Rectangle2D r = a.getBounds();
    r.setRect(r.getX() - m, r.getY() - m, r.getWidth() + (2 * m), r.getHeight() + (2 * m));
    return r;
  }

  // for convex containers
  // not using this right now but leaving for PathIterator reference
  protected static boolean contains1(Area container, Area containee) {
    if (container.contains(containee.getBounds()))
      return true;
    PathIterator pi = containee.getPathIterator(null);
    while (!pi.isDone()) {
      double[] points = new double[6];
      pi.currentSegment(points);
      if (!container.contains(points[0], points[1]))
        return false;
      pi.next();
    }
    return true;
  }

  protected static List<Line2D.Double> areaLines(Area a) {
    PathIterator pi;
    List<double[]> thePoints = new ArrayList<double[]>();
    List<Line2D.Double> theLines = new ArrayList<Line2D.Double>();
    double[] thesePoints = new double[6];
    int segType = -1;

    for (pi = a.getPathIterator(null); !pi.isDone(); pi.next()) {
      segType = pi.currentSegment(thesePoints);
      double[] thisElement = { segType, thesePoints[0], thesePoints[1] };
      thePoints.add(thisElement);
    }

    for (int i = 1; i < thePoints.size(); i++) {
      if (thePoints.get(i)[0] != PathIterator.SEG_MOVETO) {
        theLines.add(new Line2D.Double(thePoints.get(i - 1)[1], thePoints.get(i - 1)[2], thePoints.get(i)[1], thePoints.get(i)[2]));
      }
    }

    return theLines;
  }

  protected static List<Point2D.Double> areaPoints(Area a) {
    PathIterator pi = a.getPathIterator(null);
    List<Point2D.Double> thePoints = new ArrayList<Point2D.Double>();
    double[] points = new double[6];
    int asdf = 0;
    while (!pi.isDone()) {
      asdf = pi.currentSegment(points);
      System.err.print(asdf == PathIterator.SEG_MOVETO ? "!" : ".");
      thePoints.add(new Point2D.Double(points[0], points[1]));
      pi.next();
    }
    return thePoints;
  }

  protected static List<Line2D.Double> pointLines(List<Point2D.Double> l) {
    List<Line2D.Double> theLines = new ArrayList<Line2D.Double>();
    for (int i = 1; i < l.size(); i++) {
      theLines.add(new Line2D.Double(l.get(i - 1), l.get(i)));
    }
    return theLines;
  }

  protected static Point2D.Double normalize(Point2D p) {
    return normalize(p.getX(), p.getY());
  }

  protected static Point2D.Double normalize(double x, double y) {
    double magnitude = Math.sqrt((x * x) + (y * y));
    double tx = magnitude == 0 ? 0 : x / magnitude;
    double ty = magnitude == 0 ? 0 : y / magnitude;
    return new Point2D.Double(tx, ty);
  }

  protected static Point2D.Double normal(Line2D.Double l) {
    return normalize(2 * (l.y2 - l.y1), 2 * (l.x1 - l.x2));
  }

  protected static Point2D.Double midpoint(Line2D.Double l) {
    return new Point2D.Double(l.x1 + ((l.x2 - l.x1) / 2), l.y1 + ((l.y2 - l.y1) / 2));
  }

}

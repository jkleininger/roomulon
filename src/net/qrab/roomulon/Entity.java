package net.qrab.roomulon;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

abstract class Entity {

  protected Area                 theArea;

  protected Color                theFillColor = new Color(0x665500);
  protected Color                theLineColor = new Color(0xFFCC00);

  private static AffineTransform renderXform  = new AffineTransform();
  private static AffineTransform motionXform  = new AffineTransform();
  private static AffineTransform rotateXform  = new AffineTransform();
  private Point2D                axis;

  protected double               facing       = Math.PI / 2;

  protected Velocity             v            = new Velocity();

  public Entity() {
    this(new Rectangle2D.Double());
  }

  public Entity(Shape s) {
    theArea = new Area(s);
    axis = new Point2D.Double(theArea.getBounds2D().getX() + (theArea.getBounds2D().getWidth() / 2), theArea.getBounds2D().getY()
        + (theArea.getBounds2D().getHeight() / 2));
  }

  protected void rotateBy(double dtheta) {
    facing += dtheta;
    rotateXform.setToIdentity();
    rotateXform.rotate(dtheta, axis.getX(), axis.getY());
    theArea.transform(rotateXform);
  }

  protected void rotateTo(double ntheta) {
    rotateBy(ntheta - facing);
  }

  protected void rAccelerate(double r) {
    v.rAccelerate(r);
  }

  protected void rDecelerate() {
    v.rDecelerate();
  }

  protected void accelerate() {
    v.accelerate(facing);
  }

  protected void accelerate(double theta) {
    v.accelerate(theta);
  }

  protected void decelerate() {
    v.decelerate();
  }

  protected Point2D getAxis() {
    return axis;
  }

  protected void setAxis(Point2D axis) {
    this.axis = axis;
  }

  protected Velocity getV() {
    return v;
  }

  protected double getX() {
    return getCenter().getX();
  }

  protected double getY() {
    return getCenter().getX();
  }

  protected Point2D getCenter() {
    return axis;
  }

  protected void translate(double dx, double dy) {
    motionXform.setToIdentity();
    motionXform.translate(dx, dy);
    theArea.transform(motionXform);
    axis.setLocation(dx + axis.getX(), dy + axis.getY());
  }

  protected void stop() {
    v.xspeed = 0;
    v.yspeed = 0;
    v.rspeed = 0;
  }

  protected abstract Point2D getDestination();

  protected abstract void setDestination(Point2D p);

  protected void translateByFactor(double fx, double fy) {
    translate(fx * v.maxspeed, fy * v.maxspeed);
  }

  protected void setLocation(Point2D p) {
    translate(p.getX() - getCenter().getX(), p.getY() - getCenter().getY());
    axis.setLocation(p);
  }

  protected Area getArea() {
    return theArea;
  }

  protected abstract void update();

  protected Color getFillColor() {
    return theFillColor;
  }

  protected Color getLineColor() {
    return theLineColor;
  }

  protected void setFillColor(Color c) {
    theFillColor = c;
  }

  protected void setLineColor(Color c) {
    theLineColor = c;
  }

  protected Rectangle2D candidateBounds() {
    Rectangle2D dest = new Rectangle2D.Double(theArea.getBounds2D().getX() + v.xspeed, theArea.getBounds2D().getY() + v.yspeed, theArea.getBounds2D()
        .getWidth(), theArea.getBounds2D().getHeight());
    return dest;
  }

  // return a scaled and translated copy of theArea
  // for use in rendering
  protected Area scaleArea(RoomStage context) {
    renderXform.setToIdentity();
    renderXform.scale(context.factor, context.factor);
    renderXform.translate((context.getWidth() / (2 * context.factor)) - context.thePlayer.getCenter().getX(), (context.getHeight() / (2 * context.factor))
        - context.thePlayer.getCenter().getY());
    Area scaledArea = new Area(theArea);
    scaledArea.transform(renderXform);
    return scaledArea;
  }

  protected void paint(Graphics2D g, RoomStage context) {
    Area a = scaleArea(context);
    g.setColor(getFillColor());
    g.fill(a);
    g.setColor(getLineColor());
    g.draw(a);
  }

}

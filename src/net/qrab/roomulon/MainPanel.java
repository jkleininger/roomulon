package net.qrab.roomulon;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JButton;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class MainPanel extends JPanel {

  JButton            btnNew     = new JButton("New");
  JButton            btnProceed = new JButton("Proceed");
  JButton            btnQuit    = new JButton("Quit");

  ActionListener     a          = new ActionListener() {
                                  public void actionPerformed(ActionEvent e) {
                                    doButtonAction(e);
                                  }
                                };

  KeyListener        k          = new KeyListener() {
                                  public void keyPressed(KeyEvent e) {
                                    if (e.getKeyCode() <= 255)
                                      keyCodes[e.getKeyCode()] = true;
                                  }

                                  // public void keyPressed(KeyEvent e) {
                                  // System.err.print("[" + e.getKeyCode() +
                                  // "] "); }
                                  public void keyTyped(KeyEvent e) {
                                  }

                                  public void keyReleased(KeyEvent e) {
                                    if (e.getKeyCode() <= 255)
                                      keyCodes[e.getKeyCode()] = false;
                                  }
                                };

  GridBagConstraints gbc        = new GridBagConstraints();

  MapCreator         mc;
  Map                map;

  RoomStage          context;

  private boolean[]  keyCodes   = new boolean[255];

  private boolean    isRunning  = true;

  public MainPanel() {
    super();

    this.setPreferredSize(new java.awt.Dimension(1000, 700));

    this.setLayout(new GridBagLayout());

    // Room.loadTexture("bricks3.jpg");

    btnNew.addActionListener(a);
    btnProceed.addActionListener(a);
    btnQuit.addActionListener(a);

    gbc.gridwidth = 1;
    gbc.gridheight = 1;

    gbc.gridx = 0;
    gbc.gridy = 0;
    gbc.weightx = 1;
    gbc.weighty = 1;

    this.add(btnNew, gbc);

    gbc.gridx = 1;

    this.add(btnProceed, gbc);

    gbc.gridx = 2;
    gbc.gridwidth = GridBagConstraints.REMAINDER;

    this.add(btnQuit, gbc);

    this.setVisible(true);
    this.setFocusable(true);
    this.addKeyListener(k);
    this.validate();

    Thread theThread = new Thread() {
      public void run() {
        theLoop();
      }
    };
    theThread.start();
  }

  protected void update() {
    if (context != null) {
      context.processKeys(keyCodes);
      context.update();
    }
  }

  private void theLoop() {
    while (isRunning) {
      this.requestFocusInWindow();
      update();
      try {
        Thread.sleep(1);
      } catch (InterruptedException ex) {
      }
      repaint();
    }
  }

  private void doButtonAction(ActionEvent e) {
    if (e.getActionCommand().equals("New"))
      newLevel();
    else if (e.getActionCommand().equals("Proceed"))
      proceed();
  }

  private void newLevel() {
    if (mc != null)
      this.remove(mc);
    if (map == null) {
      mc = new MapCreator(900, 600);
      context = mc;
      gbc.gridx = 0;
      gbc.gridy = 1;
      gbc.weightx = 3;
      gbc.weighty = 10;
      gbc.gridwidth = GridBagConstraints.REMAINDER;
      this.add(mc, gbc);
      this.revalidate();
    }
  }

  private void proceed() {
    if (mc != null && map == null) {
      map = new Map(mc.theRooms, mc.theHalls);
      context = map;
      this.remove(mc);
      gbc.gridx = 0;
      gbc.gridy = 1;
      gbc.weightx = 3;
      gbc.weighty = 10;
      gbc.gridwidth = GridBagConstraints.REMAINDER;
      this.add(map, gbc);
      this.revalidate();
    }
  }

}

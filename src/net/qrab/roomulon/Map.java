package net.qrab.roomulon;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.util.List;

@SuppressWarnings("serial")
public class Map extends RoomStage {

  public Map() {
    super(900, 600);
    factor = 10;
    this.setBackground(new Color(0x221122));

    thePlayer = new Player(PolyShape.TRIANGLE);
    theEntities.add(thePlayer);
  }

  public Map(List<Room> r, List<Room> h) {
    this();
    setRooms(r);
    setHalls(h);
    makeBigRoom();
    if (theRooms.size() > 0)
      thePlayer.setLocation(theRooms.get(1).getCenter());
    makeMob();
  }

  protected void makeMob() {
    Mob m;
    for (Room r : theRooms) {
      m = new Mob(PolyShape.TRIANGLE);
      m.setLocation(r.getCenter());
      m.setDestination(r.randomNeighbor().getCenter());
      theEntities.add(m);
    }
  }

  @Override
  protected void processKeys(boolean[] theKeys) {
    if (theKeys[90])
      thePlayer.rAccelerate(-1);
    if (theKeys[67])
      thePlayer.rAccelerate(1);
    if (theKeys[88]) {
      thePlayer.accelerate();
    }
  }

  @Override
  protected void paintComponent(Graphics2D g) {
    g.setColor(new Color(0x040404));
    for (Line2D.Double l : lineList)
      g.draw(l);
    theBigRoom.paint(g, this);
    for (Entity theEntity : theEntities)
      theEntity.paint(g, this);
  }
}

package net.qrab.roomulon;

import java.awt.geom.Point2D;
import net.qrab.roomulon.Config;

@SuppressWarnings("serial")
class MapCreator extends RoomStage {

  private final Point2D didntMove  = new Point2D.Double();
  private Point2D separation = new Point2D.Double();
  private Point2D cohesion   = new Point2D.Double();
  private Point2D retPoint;

  private Room testRoom = new Room();

  private boolean moved;

  private double px, py;

  private int neighbors;

  public MapCreator(int wd, int ht) {
    super(wd,ht);
    //theRooms.add( new Room(440,290,120,120) );
    makeRooms(Config.maxRooms-1);
    if(Config.rotate) {
      for(Room r : theRooms) r.rotate();
    }
  }

  @Override
    protected void update() {
      if( theRooms.size() > 0 &&
          !finalized          &&
          !positionRooms() )
        finalized=true;

      if( finalized  ) {
        if(theRooms.size()<Config.maxRooms && !graphDone) {
          makeRooms(1);
          if(Config.rotate) theRooms.get(theRooms.size()-1).rotate();
          finalized=false;
        } else {
          if(!graphDone) {
            RNG();
            makeHalls();
            stageBounds = getStageBounds(10);
            makeBigRoom();
            colorLeaves();
          }
        }
      }

      try { Thread.sleep(1); } catch( InterruptedException ie ) { }
    }

  private boolean positionRooms() {
    moved = false;
    if(theRooms.size()<=1) return false;
    for(Room r : theRooms) {
      separation = getSeparation(r);
      cohesion = getCohesion(r);
      if( !separation.equals(didntMove) || !cohesion.equals(didntMove)) {
        moved = true;
        r.translate( separation.getX() , separation.getY() );
        r.translate( cohesion.getX() , cohesion.getY() );
      }
    }
    return moved;
  }

  private Point2D getCohesion(Room thisRoom) {
    px = 0;
    py = 0;
    neighbors = 0;
    for(Room thatRoom : theRooms) {
      if( !thisRoom.equals(thatRoom) ) {
        px += thatRoom.getX();
        py += thatRoom.getY();
        neighbors++;
      }
    }
    if(neighbors>0) {
      px /= neighbors;
      py /= neighbors;
    }
    px -= thisRoom.getX();
    py -= thisRoom.getY();

    retPoint = Config.normalize(px,py);

    //make sure we're not going to cause an intersection
    testRoom = new Room( thisRoom );
    testRoom.translate( retPoint.getX(), retPoint.getY() );
    for(Room thatRoom : theRooms) {
      if( !thisRoom.equals(thatRoom) &&
          thatRoom.intersects(testRoom,Config.sepMargin)) {
        retPoint.setLocation(0,0);
      }
    }

    return retPoint;
  }

  private Point2D getSeparation(Room thisRoom) {
    int    neighbors = 0;
    double px        = 0;
    double py        = 0;
    for(Room thatRoom : theRooms) {
      if( !thisRoom.equals(thatRoom) && 
          thisRoom.intersects(thatRoom,Config.sepMargin)) {
        neighbors++;
        px += thatRoom.getX() - thisRoom.getX();
        py += thatRoom.getY() - thisRoom.getY();
      }
    }
    if(neighbors>0) {
      px /= -neighbors;
      py /= -neighbors;
    }
    return Config.normalize(px,py);
  }

  //relative neighborhood graph
  void RNG() {
    double  pqDistance, pcDistance, qcDistance;
    boolean noGood;
    for(Room thisRoom : theRooms) {
      for(Room thatRoom : theRooms) {
        if(!thisRoom.equals(thatRoom)) {
          pqDistance = thisRoom.distance(thatRoom);
          noGood = false;
          for(Room candidateRoom : theRooms) {
            if( !thisRoom.equals(candidateRoom) && !thatRoom.equals(candidateRoom) ) {
              pcDistance = candidateRoom.distance(thisRoom);
              qcDistance = candidateRoom.distance(thatRoom);
              if( (pcDistance<pqDistance) && (qcDistance<pqDistance) ) {
                noGood=true;
                break;
              }
            }
          }
          if(!noGood) { thisRoom.addNeighbor(thatRoom); }
        }
      }
    }
    graphDone = true;
  }

}


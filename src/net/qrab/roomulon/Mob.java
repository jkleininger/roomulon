package net.qrab.roomulon;

import java.awt.Shape;
import java.awt.geom.Point2D;

public class Mob extends Entity {

  private Point2D destination;

  public Mob(Shape s) {
    super(s);
  }

  protected void update() {
    if(destination!=null) {
      if(destination.distance(this.getCenter())>v.maxspeed*5) {
        accelerate(facing);
      } else {
        this.stop();
        this.setLocation(destination);
        this.destination=null;
      }
    }
    if(v.xspeed!=0 || v.yspeed!=0) {
      translate(v.xspeed,v.yspeed);
    }
  }

  @Override
  protected void setDestination(Point2D p) {
    destination = new Point2D.Double(p.getX(),p.getY());
    facePoint(p);
  }

  @Override
  protected Point2D getDestination() {
    return destination;
  }

  protected void facePoint(Point2D p) {
    double theAngle = Math.atan2(this.getCenter().getY()-p.getY(),this.getCenter().getX()-p.getX());
    theAngle+=Math.PI;
    while(theAngle>(2*Math.PI)) theAngle-=Math.PI;
    rotateTo(theAngle);
  }

}

package net.qrab.roomulon;

import java.awt.Shape;
import java.awt.geom.Point2D;

public class Player extends Entity {

  public Player(Shape s) {
    super(s);
  }

  protected void update() {
    if (v.xspeed != 0 || v.yspeed != 0) {
      translate(v.xspeed, v.yspeed);
    }
    if (v.rspeed != 0) {
      rotateBy(v.rspeed);
    }
    decelerate();
    rDecelerate();
  }

  @Override
  protected Point2D getDestination() {
    return getCenter();
  }

  @Override
  protected void setDestination(Point2D p) {
    // noop
  }

}

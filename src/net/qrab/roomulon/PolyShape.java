package net.qrab.roomulon;

import java.awt.geom.Area;
import java.awt.Polygon;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

public class PolyShape extends Area {

  public static Polygon TRIANGLE = new Polygon( new int[] {-1,0,1} , new int[] {-1,1,-1} , 3);
  public static Polygon SQUARE   = new Polygon( new int[] {-1,1,1,-1}, new int[] {-1,-1,1,1} , 4);

  public PolyShape() {
    super( new Polygon( new int[] {-1,0,1} , new int[] {-1,1,-1} , 3) );
  }

  public PolyShape(Polygon p) {
    super(p);
  }

  public PolyShape(boolean b) {
    super( new Ellipse2D.Double(                -1.2, -1.2 , 2.4,  2.4  ) );
    this.add( new Area( new Rectangle2D.Double( -0.2 , 0.0 , 0.4 , 2.0  ) ) );
    this.add( new Area( new Rectangle2D.Double( -0.6 , 1.4 , 0.2 , 1.8  ) ) );
    this.add( new Area( new Rectangle2D.Double(  0.4 , 1.4 , 0.2 , 1.8  ) ) );
    this.add( new Area( new Rectangle2D.Double( -0.4 , 1.6 , 0.8 , 0.2  ) ) );
  }

}

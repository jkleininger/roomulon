package net.qrab.roomulon;

import java.awt.geom.Rectangle2D;
import java.awt.geom.Point2D;
import java.awt.geom.AffineTransform;
import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.geom.Area;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

class Room {

  private Area theArea;

  private Color fillColor = new Color(0x220011);
  private Color lineColor = new Color(0xCC0055);

  protected double neighborhood;

  protected List<Room> neighbors = new CopyOnWriteArrayList<Room>();
  protected List<Room> halls     = new CopyOnWriteArrayList<Room>();

  private static AffineTransform theTransform = new AffineTransform();

  public Room(double x, double y, double w, double h) {
    theArea = new Area( new Rectangle2D.Double(x,y,w,h));
    neighborhood = w>h?2*w/Math.sqrt(2):2*h/Math.sqrt(2);
  }

  public Room(double w, double h) {
    this(0,0,w,h);
  }

  public Room() {
    this(0,0,0,0);
  }

  public Room(Room r) {
    theArea = new Area(r.getArea());
  }

  public Room(Area a) {
    theArea=a;
  }

  protected void add(Room r) {
    theArea.add(r.getArea());
  }

  protected void addNeighbor(Room n) {
    neighbors.add(n);
  }

  protected void addHall(Room n) {
    halls.add(n);
  }

  protected boolean removeNeighbor(Room n) {
    return neighbors.remove(n);
  }

  protected double getX() {
    return theArea.getBounds2D().getX();
  }

  protected double getY() {
    return theArea.getBounds2D().getY();
  }

  protected double getWidth() {
    return theArea.getBounds2D().getWidth();
  }

  protected double getHeight() {
    return theArea.getBounds2D().getHeight();
  }

  protected Area getArea() {
    return theArea;
  }

  protected Rectangle2D getBounds() {
    return theArea.getBounds();
  }

  protected void translate(double dx, double dy) {
    AffineTransform at = new AffineTransform();
    at.translate(dx,dy);
    theArea.transform(at);
  }

  protected void rotate() {
    rotate( Config.randomRange(0 , Math.PI*2) );
  }

  protected void rotate(double theta) {
    AffineTransform at = new AffineTransform();
    Point2D center = getCenter();
    at.rotate(theta,center.getX(),center.getY());
    theArea.transform(at);
  }

  protected void scale(double factor) {
    AffineTransform at = new AffineTransform();
    at.scale(factor,factor);
    theArea.transform(at);
  }

  protected void setLocation(double x, double y) {
    translate( x-getX() , y-getY() );
  }

  protected void setLocation(Point2D location) {
    setLocation( location.getX(), location.getY());
  }

  protected Point2D getLocation() {
    return theArea.getBounds().getLocation();
  }

  protected Point2D.Double getCenter() {
    return new Point2D.Double( getX()+(getWidth()/2) , getY()+(getHeight()/2) );
  }
  protected double distance(Room r) {
    return getCenter().distance(r.getCenter());
  }

  protected boolean intersects(Room thatRoom) {
    if( (this.neighborhood + thatRoom.neighborhood) < this.getCenter().distance(thatRoom.getCenter()) ) return false;
    return Config.intersects(theArea,thatRoom.getArea());
  }

  protected boolean intersects(Room thatRoom, int margin) {
    return Config.intersects(theArea,thatRoom.getArea(),margin);
  }

  protected boolean contains(Point2D p) {
    return (
      getArea().getBounds2D().contains(p.getX(),p.getX()) &&
      getArea().contains(p.getX(),p.getY())
      );
  }

  protected boolean contains(Rectangle2D r) {
    return getArea().contains(r);
  }


  protected Room randomNeighbor() {
    return neighbors.get(Config.random.nextInt(neighbors.size()));
  }

  protected Color getFillColor() {
    return fillColor;
  }

  protected Color getLineColor() {
    return lineColor;
  }

  protected void setFillColor(Color c) {
    fillColor = c;
  }

  protected void setLineColor(Color c) {
    lineColor = c;
  }
  protected Area scaleArea(RoomStage context) {
    theTransform.setToIdentity();
    theTransform.scale(context.factor,context.factor);
    if(context.thePlayer!=null) {
      theTransform.translate(
          (context.getWidth()/(2*context.factor))-context.thePlayer.getCenter().getX(),
          (context.getHeight()/(2*context.factor))-context.thePlayer.getCenter().getY()
          );
    }
    Area scaledArea = new Area(theArea);
    scaledArea.transform(theTransform);
    return scaledArea;
  }

  protected void paint(Graphics2D g, RoomStage context) {
    g.setColor(getFillColor());
    g.fill(scaleArea(context));
    g.setColor(getLineColor());
    g.draw(scaleArea(context));
  }

}


package net.qrab.roomulon;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import javax.swing.JPanel;

@SuppressWarnings("serial")
abstract class RoomStage extends JPanel {

  protected List<Room>          theRooms;
  protected List<Room>          theHalls;
  protected List<Entity>        theEntities;
  protected List<Room>          theLeaves;

  protected Room                theBigRoom   = new Room();
  protected List<Line2D.Double> lineList;

  protected Player              thePlayer;
  protected double              factor       = 1;

  protected boolean             finalized    = false;
  protected boolean             isRunning    = true;
  protected boolean             graphDone    = false;

  protected Color               bgColor      = new Color(0x111111);

  protected Rectangle2D         stageBounds  = new Rectangle2D.Double();

  protected AffineTransform     theTransform = new AffineTransform();

  public RoomStage(int wd, int ht) {
    super();
    this.setFocusable(true);
    this.setPreferredSize(new Dimension(wd, ht));
    this.setOpaque(true);
    this.setBackground(bgColor);
    this.setVisible(true);
    this.validate();

    theRooms = new CopyOnWriteArrayList<>();
    theHalls = new CopyOnWriteArrayList<>();
    theLeaves = new CopyOnWriteArrayList<>();
    theEntities = new CopyOnWriteArrayList<>();
  }

  protected void update() {
    for (Entity e : theEntities) {
      Line2D l = bumpLine(e);
      if (l != null) {
        // Point2D p = pointRoom(e.getCenter()).getCenter();
        // e.translate(.1 * (p.getX() - e.getX()), .1 * (p.getY() - e.getY()));
        e.translate(-20 * e.getV().xspeed, -20 * e.getV().yspeed);
        e.getV().xspeed *= -1;
        e.getV().yspeed *= -1;
      }
      if (e.getDestination() == null) {
        e.setDestination(entityRoom(e).randomNeighbor().getCenter());
      }
      e.update();
    }
  }

  protected void makeRooms(int numberOfRooms) {
    for (int n = 0; n < numberOfRooms; n++) {
      theRooms.add(makeRoom());
    }
  }

  protected Room makeRoom() {
    Room r = new Room(Config.randomRange(Config.minRoom.getWidth(), Config.maxRoom.getWidth()), Config.randomRange(Config.minRoom.getHeight(),
        Config.maxRoom.getHeight()));
    moveToRandomLocation(r);
    return r;
  }

  // combine all rooms and halls into one Area
  protected void makeBigRoom() {
    theBigRoom = new Room();
    for (Room r : theRooms)
      theBigRoom.getArea().add(r.getArea());
    for (Room r : theHalls)
      theBigRoom.getArea().add(r.getArea());
    lineList = Config.areaLines(theBigRoom.getArea());
  }

  protected void moveToRandomLocation(Room r) {
    r.setLocation(Config.random.nextDouble() * (this.getPreferredSize().getWidth() - r.getWidth()), Config.random.nextDouble()
        * (this.getPreferredSize().getHeight() - r.getHeight()));
  }

  private Room nearestNeighbor(Room thisRoom) {
    double min = Double.MAX_VALUE;
    Room theNeighbor = thisRoom;
    for (Room thatRoom : theRooms) {
      if (!thisRoom.equals(thatRoom) && thisRoom.distance(thatRoom) < min) {
        min = thisRoom.distance(thatRoom);
        theNeighbor = thatRoom;
      }
    }
    return theNeighbor;
  }

  protected Line2D bumpLine(Entity e) {
    return bumpLine(e.getArea());
  }

  protected Line2D bumpLine(Area a) {
    for (Line2D l : lineList) {
      if (a.getBounds2D().intersectsLine(l))
        return l;
    }
    return null;
  }

  protected Room pointRoom(Point2D p) {
    for (Room r : theRooms) {
      if (r.getArea().contains(p))
        return r;
    }
    for (Room h : theHalls) {
      if (h.getArea().contains(p))
        return h;
    }
    return null;
  }

  // does any room fully contain e?
  protected Room entityRoom(Entity e) {
    return shapeRoom(e.getArea());
  }

  // does any room fully contain s?
  protected Room shapeRoom(Shape s) {
    for (Room r : theRooms) {
      if (r.getArea().contains(s.getBounds2D()))
        return r;
    }
    for (Room h : theHalls) {
      if (h.getArea().contains(s.getBounds2D()))
        return h;
    }
    return null;
  }

  /*
   * protected Room containingRoom(Rectangle2D r) { for(Room theRoom : theRooms)
   * if(theRoom.getArea().getBounds2D().contains(r)) return theRoom; return
   * containingHall(r); }
   * 
   * protected Room containingHall(Rectangle2D r) { for(Room theHall : theHalls)
   * if(theHall.getArea().getBounds2D().contains(r)) return theHall; return
   * null; }
   */
  protected Rectangle2D getStageBounds(double margin) {
    Rectangle2D t;
    double left = Double.MAX_VALUE;
    double top = Double.MAX_VALUE;
    double right = Double.MIN_VALUE;
    double bottom = Double.MIN_VALUE;
    for (Room r : theRooms) {
      t = r.getBounds();
      if (t.getX() < left)
        left = t.getX();
      if (t.getY() < top)
        top = t.getY();
      if (t.getX() + t.getWidth() > right)
        right = t.getX() + t.getWidth();
      if (t.getY() + t.getHeight() > bottom)
        bottom = t.getY() + t.getHeight();
    }
    return new Rectangle2D.Double(left - margin, top - margin, (right - left) + (margin * 2), (bottom - top) + (margin * 2));
  }

  private Room makeHall(Room r1, Room r2) {
    double l = r1.getCenter().distance(r2.getCenter());
    double vx = (r2.getCenter().getX() - r1.getCenter().getX()) / l;
    double vy = (r2.getCenter().getY() - r1.getCenter().getY()) / l;
    vx *= (Config.hallWd / 2);
    vy *= (Config.hallWd / 2);
    int[] xs = { (int) (r1.getCenter().getX() + vy), (int) (r2.getCenter().getX() + vy), (int) (r2.getCenter().getX() - vy), (int) (r1.getCenter().getX() - vy) };
    int[] ys = { (int) (r1.getCenter().getY() - vx), (int) (r2.getCenter().getY() - vx), (int) (r2.getCenter().getY() + vx), (int) (r1.getCenter().getY() + vx) };
    Area hall = new Area(new Polygon(xs, ys, 4));
    // hall.subtract(r1.getArea());
    // hall.subtract(r2.getArea());
    return new Room(hall);
  }

  protected void makeHalls() {
    for (Room theRoom : theRooms) {
      for (Room neighbor : theRoom.neighbors) {
        theHalls.add(makeHall(theRoom, neighbor));
        theHalls.get(theHalls.size() - 1).addNeighbor(theRoom);
        theHalls.get(theHalls.size() - 1).addNeighbor(neighbor);
        theRoom.addHall(theHalls.get(theHalls.size() - 1));
        neighbor.addHall(theHalls.get(theHalls.size() - 1));
      }
    }
  }

  protected void setRooms(List<Room> roomList) {
    theRooms = roomList;
  }

  protected void setHalls(List<Room> hallList) {
    theHalls = hallList;
  }

  protected void findLeaves() {
    theLeaves.clear();
    for (Room theRoom : theRooms) {
      if (theRoom.neighbors.size() == 1) {
        theLeaves.add(theRoom);
      }
    }
  }

  protected void colorLeaves() {
    findLeaves();
    for (Room leaf : theLeaves) {
      leaf.setFillColor(new Color(0x002200));
      leaf.setLineColor(new Color(0x229933));
    }
  }

  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    paintComponent((Graphics2D) g);
  }

  protected void paintComponent(Graphics2D g) {

    for (Room theHall : theHalls) {
      theHall.paint(g, this);
    }
    for (Room theRoom : theRooms) {
      theRoom.paint(g, this);
    }
    for (Entity theEntity : theEntities) {
      theEntity.paint(g, this);
    }
    g.draw(stageBounds);

  }

  protected void processKeys(boolean[] theKeys) {
  }
}

package net.qrab.roomulon;

public class Velocity {

  protected double xspeed;
  protected double yspeed;

  protected double rspeed;

  protected double maxspeed      = Config.maxspeed;
  protected double maxspeedsq    = maxspeed * maxspeed;

  protected double maxrspeed     = Config.rotation;

  protected double acceleration  = Config.acceleration;
  protected double deceleration  = Config.deceleration;

  protected double rAcceleration = Config.rAcceleration;
  protected double rDeceleration = Config.rDeceleration;

  public Velocity() {
  }

  protected void rAccelerate(double r) {
    if (Math.abs(rspeed) < maxrspeed) {
      rspeed += (rAcceleration * r);
    } else {
      rspeed = r * maxrspeed;
    }
  }

  protected void rDecelerate() {
    if (rspeed != 0)
      rspeed *= rDeceleration;
  }

  protected void accelerate(double theta) {
    xspeed += acceleration * Math.cos(theta);
    yspeed += acceleration * Math.sin(theta);
    double actualsq = xspeed * xspeed + yspeed * yspeed;
    if (actualsq > maxspeedsq) {
      xspeed *= (maxspeedsq / actualsq);
      yspeed *= (maxspeedsq / actualsq);
    }
  }

  protected void decelerate() {
    if (xspeed != 0)
      xspeed *= deceleration;
    if (yspeed != 0)
      yspeed *= deceleration;
  }

}

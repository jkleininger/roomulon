package net.qrab.roomulon;

import javax.swing.JFrame;

public class roomulon {

  public static void main(String arg[]) {
    javax.swing.SwingUtilities.invokeLater(new Runnable() {
       public void run() {
          JFrame theFrame = new JFrame("roomulon");
          theFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
          theFrame.setContentPane(new MainPanel());
          theFrame.pack();
          theFrame.setVisible(true);
       }
    });
  }

}
